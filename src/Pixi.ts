import * as PIXI from "pixi.js";

const app = new PIXI.Application({
  width: 1000,
  height: 500,
  transparent: false,
  backgroundColor: 0x479999
});

document.body.appendChild(app.view);

const graphics = new PIXI.Graphics();
app.stage.addChild(graphics);

const setBackground = async (imageFile: string) => {
  const texture = PIXI.Texture.fromURL(imageFile);
  const sprite = new PIXI.Sprite(await texture);
  app.stage.addChild(sprite);
};

const createGraphics = (): PIXI.Graphics => {
  const graphics = new PIXI.Graphics();
  app.stage.addChild(graphics);
  return graphics;
};

export { setBackground, createGraphics };
