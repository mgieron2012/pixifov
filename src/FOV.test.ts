// import FieldOfView, { Point } from "./FOV";

test("A", () => {
  expect(-1/0).toBe(-Infinity);
});

// import { Vector } from "./FOVUtils";

// const areVectorsEqual = (v1: Vector | Point, v2: Vector | Point) =>
//   v1.x === v2.x && v1.y === v2.y;

// test("getVectorsFromWallsApexes works", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(200, 200);

//   const vectors = fov.getVectorsFromWallsApexes(wall);
//   expect(vectors[0].apex).toEqual({ x: 10, y: 20 });
//   expect(areVectorsEqual(vectors[0].vector, new Vector(-190, -180))).toBe(true);

//   expect(vectors[1].apex).toEqual({ x: 60, y: 20 });
//   expect(areVectorsEqual(vectors[1].vector, new Vector(-140, -180))).toBe(true);

//   expect(vectors[2].apex).toEqual({ x: 60, y: 100 });
//   expect(areVectorsEqual(vectors[2].vector, new Vector(-140, -100))).toBe(true);

//   expect(vectors[3].apex).toEqual({ x: 10, y: 100 });
//   expect(areVectorsEqual(vectors[3].vector, new Vector(-190, -100))).toBe(true);
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on the right-bottom side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(200, 200);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 20 });
//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on left-bottom side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(0, 300);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 100 });
//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 20 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on left-top side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(0, 0);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 10, y: 100 });
//   expect(minAndMax.max.apex).toEqual({ x: 60, y: 20 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on right side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(120, 20);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 10, y: 20 });
//   expect(minAndMax.max.apex).toEqual({ x: 60, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on right side of apexes 2", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(120, 100);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 20 });
//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on left side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(0, 20);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 10, y: 100 });
//   expect(minAndMax.max.apex).toEqual({ x: 60, y: 20 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on left side of apexes 2", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(0, 100);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 100 });
//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 20 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on bottom side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(60, 150);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.max.apex).toEqual({ x: 60, y: 20 });
//   expect(minAndMax.min.apex).toEqual({ x: 10, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on bottom side of apexes 2", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(10, 150);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 20 });
//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on top side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(10, 0);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.max.apex).toEqual({ x: 60, y: 20 });
//   expect(minAndMax.min.apex).toEqual({ x: 10, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on top side of apexes 2", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(60, 0);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 20 });
//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on top-mid side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(30, 0);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 20 });
//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 20 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on left-mid side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(0, 50);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.max.apex).toEqual({ x: 10, y: 20 });
//   expect(minAndMax.min.apex).toEqual({ x: 10, y: 100 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on right-mid side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(100, 50);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.max.apex).toEqual({ x: 60, y: 100 });
//   expect(minAndMax.min.apex).toEqual({ x: 60, y: 20 });
// });

// test("get Vectors with Apexes with minimum and maximum A, player is on bottom-mid side of apexes", () => {
//   const wall = { x: 10, y: 20, width: 50, height: 80 };

//   const fov = new FieldOfView(1000, 500, [wall]);

//   fov.updatePlayerPosition(30, 150);

//   const vectors = fov.getVectorsFromWallsApexes(wall);

//   const minAndMax = fov.getVectorsWithApexesWithMinMaxA(vectors);

//   expect(minAndMax.min.apex).toEqual({ x: 10, y: 100 });
//   expect(minAndMax.max.apex).toEqual({ x: 60, y: 100 });
// });

// test("is point on map border", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   expect(fov.map.isPointOnEdge({ x: 0, y: 0 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 0, y: 150 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 0, y: 500 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 1000, y: 0 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 1000, y: 450 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 1000, y: 500 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 150, y: 0 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 999, y: 0 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: 370, y: 500 })).toBe(true);
//   expect(fov.isPointOnMapBorder({ x: -1, y: 500 })).toBe(false);
//   expect(fov.isPointOnMapBorder({ x: 1001, y: 500 })).toBe(false);
//   expect(fov.isPointOnMapBorder({ x: -1, y: 0 })).toBe(false);
//   expect(fov.isPointOnMapBorder({ x: 1005, y: 0 })).toBe(false);
//   expect(fov.isPointOnMapBorder({ x: 1, y: 1 })).toBe(false);
//   expect(fov.isPointOnMapBorder({ x: -500, y: -500 })).toBe(false);
// });

// test("getPointOfVectorAndMapIntersection A is 0", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = { apex: { x: 100, y: 100 }, vector: new Vector(1, 0) };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 1000, y: 100 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection A is -0", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = {
//     apex: { x: 100, y: 100 },
//     vector: new Vector(-1, 0),
//   };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 0, y: 100 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection A is Infinity", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = { apex: { x: 100, y: 100 }, vector: new Vector(0, 1) };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 100, y: 500 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection A is -Infinity", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = {
//     apex: { x: 100, y: 100 },
//     vector: new Vector(0, -1),
//   };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 100, y: 0 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection bottom border", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = {
//     apex: { x: 100, y: 100 },
//     vector: new Vector(1, 10),
//   };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 140, y: 500 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection top-left border", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = {
//     apex: { x: 100, y: 100 },
//     vector: new Vector(-1, -1),
//   };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 0, y: 0 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection top border", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = {
//     apex: { x: 300, y: 100 },
//     vector: new Vector(1, -1),
//   };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 400, y: 0 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection right border", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = {
//     apex: { x: 300, y: 100 },
//     vector: new Vector(100, 1),
//   };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 1000, y: 107 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection left border", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = {
//     apex: { x: 300, y: 100 },
//     vector: new Vector(-100, -1),
//   };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 0, y: 97 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("getPointOfVectorAndMapIntersection right-bottom border", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const vectorWithApex = { apex: { x: 0, y: 0 }, vector: new Vector(1, 0.5) };
//   const result = fov.getPointOfVectorAndMapIntersection(vectorWithApex);
//   const expected = { x: 1000, y: 500 };
//   expect(areVectorsEqual(expected, result)).toBe(true);
// });

// test("arePointsOnSameSideOfMap", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const corner1 = { x: 0, y: 0 },
//     corner2 = { x: 1000, y: 500 },
//     left = { x: 0, y: 300 },
//     top = { x: 150, y: 0 },
//     right = { x: 1000, y: 150 },
//     bottom = { x: 200, y: 500 };

//   expect(fov.arePointsOnSameSideOfMap(corner1, left)).toBe(true);
//   expect(fov.arePointsOnSameSideOfMap(right, top)).toBe(false);
//   expect(fov.arePointsOnSameSideOfMap(corner2, top)).toBe(false);
//   expect(fov.arePointsOnSameSideOfMap(bottom, top)).toBe(false);
// });

// test("arePointsOnOppositeSidesOfMap", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const corner1 = { x: 0, y: 0 },
//     corner2 = { x: 1000, y: 0 },
//     left = { x: 0, y: 300 },
//     top = { x: 150, y: 0 },
//     right = { x: 1000, y: 150 },
//     bottom = { x: 200, y: 500 };

//   expect(fov.arePointsOnOppositeSidesOfMap(corner1, left)).toBe(false);
//   expect(fov.arePointsOnOppositeSidesOfMap(right, top)).toBe(false);
//   expect(fov.arePointsOnOppositeSidesOfMap(bottom, top)).toBe("Y");
//   expect(fov.arePointsOnOppositeSidesOfMap(left, right)).toBe("X");
//   expect(fov.arePointsOnOppositeSidesOfMap(corner1, corner2)).toBe(false);
// });

// test("getCornersToDrawPoints opposite X", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   fov.updatePlayerPosition(500, 350);
//   const pointsToDraw = [
//     { x: 0, y: 100 },
//     { x: 1000, y: 200 },
//     { x: 100, y: 300 },
//     { x: 500, y: 300 },
//   ];
//   const expected = [
//     { x: 0, y: 0 },
//     { x: 1000, y: 0 },
//   ];

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toStrictEqual(expected);
// });

// test("getCornersToDrawPoints opposite X 2", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const pointsToDraw = [
//     { x: 0, y: 100 },
//     { x: 1000, y: 200 },
//     { x: 100, y: 300 },
//     { x: 500, y: 300 },
//   ];
//   const expected = [
//     { x: 0, y: 0 },
//     { x: 1000, y: 0 },
//   ];

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toStrictEqual(expected);
// });

// test("getCornersToDrawPoints opposite Y", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   fov.updatePlayerPosition(50, 200);
//   const pointsToDraw = [
//     { x: 100, y: 0 },
//     { x: 300, y: 300 },
//     { x: 200, y: 500 },
//     { x: 400, y: 400 },
//   ];
//   const expected = [
//     { x: 1000, y: 0 },
//     { x: 1000, y: 500 },
//   ];

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toStrictEqual(expected);
// });

// test("getCornersToDrawPoints opposite Y 2", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   fov.updatePlayerPosition(600, 350);
//   const pointsToDraw = [
//     { x: 100, y: 0 },
//     { x: 300, y: 300 },
//     { x: 200, y: 500 },
//     { x: 400, y: 400 },
//   ];
//   const expected = [
//     { x: 0, y: 0 },
//     { x: 0, y: 500 },
//   ];

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toStrictEqual(expected);
// });

// test("getCornersToDrawPoints add top-left", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const pointsToDraw = [
//     { x: 0, y: 100 },
//     { x: 100, y: 0 },
//     { x: 80, y: 80 },
//     { x: 70, y: 70 },
//   ];
//   const expected = { x: 0, y: 0 };

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toContainEqual(expected);
// });

// test("getCornersToDrawPoints add top-right", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const pointsToDraw = [
//     { x: 800, y: 0 },
//     { x: 100, y: 10 },
//     { x: 1000, y: 80 },
//     { x: 70, y: 70 },
//   ];
//   const expected = { x: 1000, y: 0 };

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toContainEqual(expected);
// });

// test("getCornersToDrawPoints add bottom-right", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const pointsToDraw = [
//     { x: 1000, y: 100 },
//     { x: 100, y: 500 },
//     { x: 80, y: 80 },
//     { x: 70, y: 70 },
//   ];
//   const expected = { x: 1000, y: 500 };

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toContainEqual(expected);
// });

// test("getCornersToDrawPoints add bottom-left", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const pointsToDraw = [
//     { x: 0, y: 100 },
//     { x: 100, y: 500 },
//     { x: 80, y: 80 },
//     { x: 70, y: 70 },
//   ];
//   const expected = { x: 0, y: 500 };

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toContainEqual(expected);
// });

// test("getCornersToDrawPoints opposite X", () => {
//   const fov = new FieldOfView(1000, 500, []);
//   const pointsToDraw = [
//     { x: 400, y: 400 },
//     { x: 600, y: 400 },
//     { x: 1000, y: 275 },
//     { x: 0, y: 280 },
//   ];
//   const expected = [
//     { x: 0, y: 0 },
//     { x: 1000, y: 0 },
//   ];

//   expect(fov.getCornersToDrawPoints(pointsToDraw)).toStrictEqual(expected);
// });
