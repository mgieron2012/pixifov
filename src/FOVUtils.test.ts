import { Wall } from "./FOV";
import { getWallApexes, Vector } from "./FOVUtils";

test("test", () => {
  expect(true).toBe(true);
});

test("creates apexes", () => {
  const wall: Wall = { x: 100, y: 10, width: 25, height: 30 };
  expect(getWallApexes(wall)).toStrictEqual([
    { x: 100, y: 10 },
    { x: 125, y: 10 },
    { x: 125, y: 40 },
    { x: 100, y: 40 },
  ]);
});

test("creates vector from points", () => {
  const vector = new Vector({ x: 10, y: 0 }, { x: 10, y: 35 });
  expect(vector.x).toBe(0)
  expect(vector.y).toBe(35)
});

test("creates vector from points 2", () => {
  const vector = new Vector({ x: -59, y: -56 }, { x: 50, y: 56 });
  expect(vector.x).toBe(109)
  expect(vector.y).toBe(112)
});


test("get vectors linear A", () => {
  const vector = new Vector({x: 0, y: 20}, {x: 1, y: 40});
  expect(vector.direction).toBe(20)
});

test("get vectors linear A with result as Infinity", () => {
  const vector = new Vector({x: 0, y: 20}, {x: 0, y: 0});
  expect(vector.direction).toBe(Infinity)
});

