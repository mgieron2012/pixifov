import { Graphics } from "pixi.js";
import { createGraphics } from "./Pixi";

export class Wall {
  private x: number;
  private y: number;
  private width: number;
  private height: number;
  private graphics: Graphics;
  private color: number;

  constructor(
    x: number,
    y: number,
    width: number,
    height: number,
    color = 0x000000
  ) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.color = color;

    this.graphics = createGraphics();
    this.graphics.interactive = true;
    this.graphics.buttonMode = true;
    this.graphics.on("pointerdown", () => {});
  }

  static createFromMouseEvents(click1: MouseEvent, click2: MouseEvent): Wall {
    let x = 0,
      y = 0,
      width = 0,
      height = 0;

    if (click1.pageX > click2.pageX) {
      x = click1.pageX;
      width = click2.pageX - click1.pageX;
    } else {
      x = click2.pageX;
      width = click1.pageX - click2.pageX;
    }

    if (click1.pageY > click2.pageY) {
      y = click1.pageY;
      height = click2.pageY - click1.pageY;
    } else {
      y = click2.pageY;
      height = click1.pageY - click2.pageY;
    }

    return new Wall(x, y, width, height);
  }

  draw = () => {
    const { x, y, width, height, color, graphics } = this;
    graphics.clear();
    graphics.beginFill(color);
    graphics.drawRect(x, y, width, height);
    graphics.endFill();
  };
}
