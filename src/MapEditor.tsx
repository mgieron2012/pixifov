import React, { useEffect } from "react";
import { Wall } from "./Wall";
import FOV from "./FOV";

export function MapEditor() {
  useEffect(() => {
    const fov = new FOV(1000, 500, [
      { x: 400, y: 200, width: 100, height: 100 },
      // { x: 0, y: 0, width: 50, height: 50 },
      // { x: 700, y: 400, width: 30, height: 20 },
    ]);

    const wall = new Wall(400, 200, 100, 100, 0x153698);
    wall.draw();
    // const wall2 = new Wall(0, 0, 50, 50, 0x153698);
    // wall2.draw();
    // const wall3 = new Wall(700, 400, 30, 20, 0x153698);
    // wall3.draw();

    document.body.addEventListener("click", (event) => {
      fov.updatePlayerPosition(event.pageX, event.pageY);
    });

  }, []);

  return <div />;
}
