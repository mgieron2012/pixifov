import { createGraphics } from "./Pixi";
import * as PIXI from "pixi.js";

import { getWallApexes, Map, Vector } from "./FOVUtils";

export type Wall = {
  x: number;
  y: number;
  width: number;
  height: number;
};

export type Point = { x: number; y: number };

export default class FieldOfView {
  private mapWidth: number;
  private mapHeight: number;
  private map: Map;
  private playerPosition: Point;
  private graphics: PIXI.Graphics;
  private walls: Array<Wall>;

  constructor(mapWidth: number, mapHeight: number, walls: Array<Wall>) {
    this.mapHeight = mapHeight;
    this.mapWidth = mapWidth;
    this.playerPosition = { x: 0, y: 0 };
    this.graphics = createGraphics();
    this.walls = walls;
    this.map = new Map(mapWidth, mapHeight);
  }

  updatePlayerPosition(newPositionX: number, newPositionY: number) {
    this.playerPosition.x = newPositionX;
    this.playerPosition.y = newPositionY;
    this.updateShadows();
  }

  updateShadows() {
    const startTime = Date.now();
    this.graphics.clear();
    this.walls.forEach((wall) => this.updateShadow(wall));
    console.log(`FOV - Czas trwania: ${Date.now() - startTime} milisekund`);
  }

  updateShadow(wall: Wall) {
    const apexesWithVectors = this.getVectorsFromWallsApexes(wall);
    const apexesWithVectorsWithMinAndMaxA = this.getVectorsWithMinMaxDirection(
      apexesWithVectors
    );
    let pointsToDraw = [
      apexesWithVectorsWithMinAndMaxA.min.b,
      apexesWithVectorsWithMinAndMaxA.max.b,
      this.map.getPointOfVectorAndMapEdgeIntersection(
        apexesWithVectorsWithMinAndMaxA.max
      ),
      this.map.getPointOfVectorAndMapEdgeIntersection(
        apexesWithVectorsWithMinAndMaxA.min
      ),
    ];
    pointsToDraw = this.includeCornersToPointsToDraw(pointsToDraw);
    this.graphics.beginFill(0x000000);
    this.graphics.drawPolygon(pointsToDraw as Array<PIXI.Point>);
    this.graphics.endFill();
  }

  getVectorsFromWallsApexes = (wall: Wall): Array<Vector> =>
    getWallApexes(wall).map((apex) => 
        new Vector(this.playerPosition, apex)
);

  getVectorsWithMinMaxDirection = (
    vectors: Array<Vector>
  ) => {
    const hasInfinity = vectors.some(
      (vector) => vector.direction === Infinity
    );
    const hasMinusInfinity = vectors.some(
      (vector) => vector.direction === -Infinity
    );

    let max: Vector,
      min = (max = vectors[0]);
    vectors.forEach((vector) => {
      const directionOfCurrent = vector.direction;
      if (
        (hasMinusInfinity &&
          Math.abs(directionOfCurrent) < Math.abs(max.direction)) ||
        (!hasMinusInfinity &&
          (directionOfCurrent > max.direction ||
            (directionOfCurrent === max.direction &&
              (Math.abs(max.x) < Math.abs(vector.x) ||
                Math.abs(max.y) < Math.abs(vector.y)))))
      ) {
        max = vector;
      }
      if (
        (!hasInfinity &&
          (directionOfCurrent < min.direction ||
            (directionOfCurrent === min.direction &&
              (Math.abs(min.x) < Math.abs(vector.x) ||
                Math.abs(min.y) < Math.abs(vector.y))))) ||
        (hasInfinity && Math.abs(directionOfCurrent) < Math.abs(min.direction))
      ) {
        min = vector;
      }
    });

    if (
      (this.playerPosition.x - min.b.x) *
        (this.playerPosition.x - max.b.x) <
      0
    ) {
      const result = vectors.filter(
        (vector) =>
          (vector.b.x !== min.b.x ||
            vector.b.y !== min.b.y) &&
          (vector.b.x !== max.b.x ||
            vector.b.y !== max.b.y)
      );
      min = result[0];
      max = result[1];
    }
    return { max: min, min: max };
  };

  includeCornersToPointsToDraw = (pointsToDraw: Array<Point>) => {
    const cornersToInclude = this.getCornersToDrawPoints(pointsToDraw);

    return pointsToDraw
      .slice(0, 3)
      .concat(cornersToInclude, pointsToDraw.slice(3, 4));
  };

  getCornersToDrawPoints = (points: Array<Point>): Array<Point> => {
    let borderPoints = points.filter((point) => this.map.isPointOnEdge(point));

    if (borderPoints[0].x !== 0 && borderPoints[0].y !== 0)
      borderPoints = borderPoints.reverse();

    const nonBorderPoints = points.filter(
      (point) => !this.map.isPointOnEdge(point)
    );
    if (
      this.map.arePointsOnSameEdge(borderPoints[0], borderPoints[1]) ||
      borderPoints.length < 2 ||
      nonBorderPoints.length === 0
    )
      return [];
    if (
      this.map.arePointsOnOppositeEdges(borderPoints[0], borderPoints[1]) ===
      "X"
    ) {
      if (
        new Vector(borderPoints[0], borderPoints[1]).direction *
          nonBorderPoints[0].x +
          borderPoints[0].y <
        nonBorderPoints[0].y
      )
        return [
          { x: this.mapWidth, y: 0 },
          { x: 0, y: 0 },
        ];
      else
        return [
          { x: 0, y: this.mapHeight },
          { x: this.mapWidth, y: this.mapHeight },
        ];
    } else if (
      this.map.arePointsOnOppositeEdges(borderPoints[0], borderPoints[1]) ===
      "Y"
    ) {
      if (
        new Vector(borderPoints[0], borderPoints[1]).direction /
          nonBorderPoints[0].y +
          borderPoints[0].x <
        nonBorderPoints[0].x
      )
        return [
          { x: 0, y: this.mapHeight },
          { x: 0, y: 0 },
        ];
      else
        return [
          { x: this.mapWidth, y: 0 },
          { x: this.mapWidth, y: this.mapHeight },
        ];
    }

    return [
      {
        x:
          borderPoints[0].x === 0 || borderPoints[1].x === 0
            ? 0
            : this.mapWidth,
        y:
          borderPoints[0].y === 0 || borderPoints[1].y === 0
            ? 0
            : this.mapHeight,
      },
    ];
  };
}
