import { Point, Wall } from "./FOV";

export const getWallApexes = (wall: Wall) => {
  return [
    { x: wall.x, y: wall.y },
    { x: wall.x + wall.width, y: wall.y },
    { x: wall.x + wall.width, y: wall.y + wall.height },
    { x: wall.x, y: wall.y + wall.height },
  ];
};

export class Vector {
  a: Point;
  b: Point;

  constructor(a: Point, b: Point) {
    this.a = a;
    this.b = b;
  }

  get x() {
    return this.b.x - this.a.x;
  }

  get y() {
    return this.b.y - this.a.y;
  }

  get direction() {
    return this.y / this.x;
  }
}

export class Map {
  width: number;
  height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  getPointOfVectorAndMapEdgeIntersection = (vector: Vector): Point => {
    let result = { x: 0, y: 0 };

    if (vector.x > 0) {
      result.x = this.width;
      result.y = (this.width - vector.b.x) * vector.direction + vector.b.y;
    } else {
      result.x = 0;
      result.y = -vector.b.x * vector.direction + vector.b.y;
    }
    if (this.isPointOnEdge(result)) return result;

    if (vector.y > 0) {
      result.x = (this.height - vector.b.y) / vector.direction + vector.b.x;
      result.y = this.height;
    } else {
      result.x = -vector.b.y / vector.direction + vector.b.x;
      result.y = 0;
    }
    return result;
  };

  isPointOnEdge = (point: Point): boolean =>
    ((point.x === 0 || point.x === this.width) &&
      point.y >= 0 &&
      point.y <= this.height) ||
    ((point.y === 0 || point.y === this.height) &&
      point.x >= 0 &&
      point.x <= this.width);

  arePointsOnSameEdge = (p1: Point, p2: Point) =>
    (p1.x === p2.x && (p1.x === 0 || p1.x === this.width)) ||
    (p1.y === p2.y && (p1.y === 0 || p1.y === this.height));

  arePointsOnOppositeEdges = (p1: Point, p2: Point) => {
    if (this.arePointsOnSameEdge(p1, p2)) return false;
    if (
      (p1.x === 0 && p2.x === this.width) ||
      (p2.x === 0 && p1.x === this.width)
    )
      return "X";
    if (
      (p1.y === 0 && p2.y === this.height) ||
      (p2.y === 0 && p1.y === this.height)
    )
      return "Y";
    return false;
  };
}
